use reqwest::StatusCode;
use std::collections::HashMap;
use std::error;

use crate::env::base_url_string;
use crate::models::User;
use crate::service::Token;
use crate::utils::request;

#[derive(Debug)]
pub struct UserApi<'a>(&'a Token);

impl<'a> UserApi<'a> {
    pub fn from(token: &'a Token) -> Self {
        Self(&token)
    }

    pub fn auth_info(&self) -> Result<User, Box<dyn error::Error>> {
        let response = request()
            .get(base_url_string(Some("/api/auth")))
            .headers(self.0.headers())
            .send()?;

        let user_data: User = response.json::<User>()?;

        Ok(user_data)
    }

    pub fn auth_update(&self, params: &HashMap<&str, &str>) -> Result<(), Box<dyn error::Error>> {
        let response = request()
            .put(base_url_string(Some("/api/auth")))
            .form(&params)
            .headers(self.0.headers())
            .send()?;

        if response.status() == StatusCode::UNPROCESSABLE_ENTITY {
            let data: serde_json::Value =
                serde_json::from_str(response.text().unwrap().as_str()).unwrap();

            return Err(data["message"]
                .as_str()
                .unwrap_or("UNPROCESSABLE_ENTITY")
                .into());
        }

        Ok(())
    }
}
