use chrono::Local;

use super::file;
use crate::env::storage_path;

struct LogType(&'static str);

impl std::ops::Deref for LogType {
    type Target = &'static str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

pub struct Log;

impl Log {
    fn print(log_type: LogType, message: &str) {
        let message = format!("{} {}", *log_type, message);

        if *log_type != "DEBUG" || cfg!(debug_assertions) {
            println!("{}", message);
        }

        if *log_type != "DEBUG" {
            Self::append_file("output.log", &message);
        }
    }

    fn eprint(log_type: LogType, message: &str) {
        let message = format!("{} {}", *log_type, message);

        eprintln!("{}", message);

        Self::append_file("error.log", &message);
    }

    fn append_file(path: &str, message: &str) {
        let log_path = storage_path(Some(path));

        let timestamp: String = Local::now().format("%Y-%M-%dT%H:%M:%S%.3f%z").to_string();

        let _ = file::append_file(
            log_path.to_str().unwrap(),
            format!("{} : {}", &timestamp, &message).as_str(),
        );
    }

    pub fn ok(message: &str) {
        Self::print(LogType("OK"), &message);
    }

    pub fn info(message: &str) {
        Self::print(LogType("INFO"), &message);
    }

    pub fn warn(message: &str) {
        Self::print(LogType("WARN"), &message);
    }

    pub fn debug(message: &str) {
        Self::print(LogType("DEBUG"), &message);
    }

    pub fn fail(message: &str) {
        Self::eprint(LogType("FAIL"), &message);
    }
}
