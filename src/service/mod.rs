mod api;
mod credentials;
mod token;

use reqwest::StatusCode;
use serde_json;
use std::error;

use crate::env::base_url_string;
use crate::models::User;
use crate::utils::request;
use api::UserApi;
pub use credentials::Credentials;
use token::{Token, TokenFile};

#[derive(Debug)]
pub struct RestApi {
    auth: User,
    token: Token,
    credentials: Credentials,
}

impl RestApi {
    pub fn from(username: &str, password: &str) -> Result<RestApi, Box<dyn error::Error>> {
        let credentials: Credentials = Credentials::from(&username, &password);

        let token: Token = match TokenFile::read_token(&credentials) {
            Some(value) => value,
            None => {
                let value = Self::request_token(&credentials)?;

                TokenFile::write_token(&credentials, &value);

                value
            }
        };

        let auth: User = UserApi::from(&token).auth_info()?;

        Ok(Self {
            auth,
            token,
            credentials,
        })
    }

    fn request_token(credentials: &Credentials) -> Result<Token, Box<dyn error::Error>> {
        let Credentials { username, password } = credentials;

        let params = [("login", username), ("password", password)];

        let response = request()
            .post(base_url_string(Some("/api/token/login")))
            .form(&params)
            .send()?;

        if response.status() == StatusCode::UNPROCESSABLE_ENTITY {
            let data: serde_json::Value =
                serde_json::from_str(response.text().unwrap().as_str()).unwrap();

            return Err(data["message"]
                .as_str()
                .unwrap_or("UNPROCESSABLE_ENTITY")
                .into());
        } else if response.status() != StatusCode::OK {
            return Err(response.status().as_str().into());
        }

        let data: serde_json::Value = response.json::<serde_json::Value>()?;

        let token_value = data["_token"]
            .as_str()
            .ok_or("Parameter '_token' not found in /login response message.")?;

        Ok(Token::from(token_value, None))
    }

    pub fn refresh_token(&mut self) -> Result<(), Box<dyn error::Error>> {
        let token = Self::request_token(&self.credentials)?;

        TokenFile::write_token(&self.credentials, &token);

        Ok(())
    }

    pub fn get_auth(&self) -> &User {
        &self.auth
    }

    pub fn set_auth(&mut self, user: &User) {
        self.auth = user.clone()
    }

    pub fn user(&self) -> UserApi {
        UserApi::from(&self.token)
    }
}
