use super::input;
use crate::env::ENV;
use crate::service::RestApi;
use crate::utils::Log;

pub fn connect_to_service() -> Option<RestApi> {
    let mut first_attempt: bool = true;

    loop {
        let error_message = match authenticate(first_attempt) {
            Ok(value) => return Some(value),
            Err(error) => error
        };

        Log::fail(error_message.as_str());

        let to_repeat = inquire::Confirm::new("Repeat authorization attempt?")
            .with_default(true)
            .prompt()
            .unwrap_or(false);

        if to_repeat {
            first_attempt = false;

            continue;
        }

        return None;
    }
}

fn authenticate(from_env: bool) -> Result<RestApi, String> {
    let username = if from_env && ENV.app_username.len() > 0 {
        Log::debug("Using username from variables.");

        ENV.app_username.to_owned()
    } else {
        let input_result =
            input::input_not_empty("Enter username:", Some("Username should not be empty."))
                .ok_or_else(|| "Auth canceled due to empty username.")?;

        input_result
    };

    let password = if from_env && ENV.app_password.len() > 0 {
        Log::debug("Using password from variables.");

        ENV.app_password.to_owned()
    } else {
        let input_result =
            input::password_not_empty("Enter password:", Some("Password should not be empty"))
                .ok_or_else(|| "Auth canceled due to empty password.")?;

        input_result
    };

    RestApi::from(&username, &password).map_err(|error| error.to_string())
}
