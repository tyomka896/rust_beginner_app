use reqwest::{ self, blocking };

pub fn request() -> blocking::Client {
    blocking::Client::new()
}
