use url::Url;
use std::{ env, path };
use once_cell::sync::Lazy;
use std::collections::HashMap;

#[derive(Debug)]
pub struct Env {
    pub app_url: Url,
    pub app_username: String,
    pub app_password: String,
    pub storage_path: path::PathBuf,
}

fn check_storage_exists(path: &path::PathBuf) {
    if std::path::Path::new(&path).exists() {
        return;
    }

    if let Err(error) = std::fs::create_dir_all(&path) {
        println!("WARN Unable to create storage due: {}", error);
    }
    else {
        println!("INFO Directory for storage created");
    }
}

impl Env {
    pub fn new() -> Self {
        let is_dev = cfg!(debug_assertions);

        let env_name = if is_dev { ".env.dev" } else { ".env" };

        dotenv::from_filename(env_name).ok();

        let variables: HashMap<String, String> = env::vars().collect();

        let mut app_url: Url = Url::parse("http://localhost").unwrap();

        if let Some(value) = variables.get("APP_URL") {
            if let Ok(url_value) = Url::parse(&value) {
                app_url = url_value;
            }
        }

        let app_username: String = variables.get("APP_USERNAME")
            .cloned()
            .unwrap_or_default();

        let app_password: String = variables.get("APP_PASSWORD")
            .cloned()
            .unwrap_or_default();

        let storage = if is_dev { "src/storage" } else { "storage" };
        let storage_path = path::PathBuf::from(&storage);

        check_storage_exists(&storage_path);

        Env {
            app_url,
            app_username,
            app_password,
            storage_path,
        }
    }
}

pub static ENV: Lazy<Env> = Lazy::new(Env::new);

pub fn base_url_string(path: Option<&str>) -> String {
    let mut result = ENV.app_url.clone();

    if let Some(value) = path {
        result = result.join(value).unwrap();
    }

    result.as_ref().to_owned()
}

pub fn storage_path(path: Option<&str>) -> path::PathBuf {
    let result = ENV.storage_path.clone().canonicalize().unwrap();

    if let Some(value) = path {
        return result.join(value);
    }

    result
}
