use inquire::{InquireError, Select};

pub fn select_option(question: &str, options: Vec<&str>) -> Option<String> {
    let options_size = options.len();

    let answer: Result<&str, InquireError> = Select::new(question, options)
        .with_page_size(options_size)
        .with_help_message("↑↓ move, to select")
        .prompt();

    match answer {
        Ok(value) => Some(value.to_owned()),
        Err(_) => None,
    }
}

#[derive(Debug, Clone)]
pub struct SelectItem<'a, T>(&'a T, &'a str);

impl<'a, T> std::fmt::Display for SelectItem<'a, T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.1)
    }
}

pub fn select_in_dev<T>(question: &str, options: Vec<(T, &str)>) -> Option<T>
where
    T: std::fmt::Display + Clone,
{
    let menu_options: Vec<SelectItem<T>> = options
        .iter()
        .map(|elem| SelectItem(&elem.0, elem.1))
        .collect();

    let options_size = options.len();

    let answer: Result<SelectItem<T>, InquireError> = Select::new(&question, menu_options)
        .with_page_size(options_size)
        .with_help_message("↑↓ move, to select")
        .prompt();

    match answer {
        Ok(value) => Some(value.0.clone()),
        Err(_) => None,
    }
}
