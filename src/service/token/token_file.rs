use md5;
use std::path;

use super::Token;
use crate::env::storage_path;
use crate::service::Credentials;
use crate::utils::{file, Log};

#[derive(Debug)]
pub struct TokenFile;

impl TokenFile {
    fn token_file_name(credentials: &Credentials) -> String {
        let Credentials { username, password } = &credentials;

        let name_md5 = md5::compute(format!("{}:{}", username, password));

        format!("{:x}.json", name_md5)
    }

    pub fn read_token(credentials: &Credentials) -> Option<Token> {
        let token_file_name: String = Self::token_file_name(&credentials);

        let token_path: path::PathBuf = storage_path(None).join(&token_file_name);

        if !token_path.exists() {
            return None;
        }

        let token_path_str = token_path.to_str().unwrap();

        file::read_json::<Token>(&token_path_str)
            .or_else(|error| {
                Log::debug(&format!("Unable to read token from: {}", token_path_str));

                Err(error)
            })
            .ok()
    }

    pub fn write_token(credentials: &Credentials, token: &Token) {
        let token_file_name: String = Self::token_file_name(&credentials);

        let token_path: path::PathBuf = storage_path(None).join(&token_file_name);

        let _ =
            file::write_json::<Token>(&token_path.to_str().unwrap(), &token).map_err(|error| error);
    }
}

#[cfg(test)]
mod tests {
    use super::TokenFile;
    use crate::service::Credentials;

    #[test]
    fn token_file_name_as_md5() {
        let credentials: Credentials = Credentials::from("username", "password");

        let name_md5: String = TokenFile::token_file_name(&credentials);

        assert_eq!(name_md5, "133e1b8eda335c4c7f7a508620ca7f10.json");
    }
}
