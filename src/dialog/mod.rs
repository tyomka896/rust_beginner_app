mod connect;
pub mod input;
pub mod select;

use std::collections::HashMap;

use crate::{service::RestApi, utils::Log};
pub use connect::connect_to_service;

#[derive(Debug, Clone)]
enum MainMenu {
    FirstName,
    LastName,
    Patronym,
    FullName,
    Exit,
}

impl std::fmt::Display for MainMenu {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self)
    }
}

pub fn main_user_menu(rest_api: &RestApi) {
    let question: &str = "Select change action:";

    let options: Vec<(MainMenu, &str)> = vec![
        (MainMenu::FirstName, "First name"),
        (MainMenu::LastName, "Second name"),
        (MainMenu::Patronym, "Patronym"),
        (MainMenu::FullName, "Full name"),
        (MainMenu::Exit, "🚪 Exit"),
    ];

    loop {
        let answer: MainMenu = match select::select_in_dev(&question, options.clone()) {
            Some(value) => value,
            None => break,
        };

        match answer {
            MainMenu::FirstName => update_first_name(&rest_api),
            MainMenu::LastName => update_last_name(&rest_api),
            MainMenu::Patronym => update_patronym(&rest_api),
            MainMenu::FullName => full_name(&rest_api),
            MainMenu::Exit => break,
        }
    }
}

fn update_first_name(rest_api: &RestApi) {
    let value = match input::input_not_empty(
        "Enter new first name:",
        Some("First name cannot be empty."),
    ) {
        Some(value) => value,
        None => return,
    };

    let mut params: HashMap<&str, &str> = HashMap::new();
    params.insert("first_name", &value);

    match rest_api.user().auth_update(&params) {
        Ok(_) => Log::info("First name successful updated."),
        Err(error) => Log::fail(format!("Unable update first name {}", error).as_str()),
    };
}

fn update_last_name(rest_api: &RestApi) {
    let value = match input::input_not_empty(
        "Enter new last name:",
        Some("Last name cannot be empty."),
    ) {
        Some(value) => value,
        None => return,
    };

    let mut params: HashMap<&str, &str> = HashMap::new();
    params.insert("last_name", &value);

    match rest_api.user().auth_update(&params) {
        Ok(_) => Log::info("Last name successful updated."),
        Err(error) => Log::fail(format!("Unable update last name {}", error).as_str()),
    };
}

fn update_patronym(rest_api: &RestApi) {
    let value = match input::input_not_empty(
        "Enter new patronym:",
        Some("Patronym cannot be empty."),
    ) {
        Some(value) => value,
        None => return,
    };

    let mut params: HashMap<&str, &str> = HashMap::new();
    params.insert("patronym", &value);

    match rest_api.user().auth_update(&params) {
        Ok(_) => Log::info("Patronym successful updated."),
        Err(error) => Log::fail(format!("Unable update patronym {}", error).as_str()),
    };
}

fn full_name(rest_api: &RestApi) {
    let user = match rest_api.user().auth_info() {
        Ok(value) => value,
        Err(error) => {
            Log::fail(format!("Unable to retrieve user info - {}", error).as_str());

            return;
        }
    };

    Log::info(format!("User full name: {}", user.full_name()).as_str());
}
