use serde::{Deserialize, Serialize};
use serde_json;
use std::error;
use std::{
    fs,
    io::{self, Write},
};

pub fn create_file(path: &str) -> io::Result<()> {
    write_file(&path, "")?;

    Ok(())
}

pub fn create_all_dir(path: &str) -> io::Result<()> {
    fs::create_dir_all(&path)?;

    Ok(())
}

pub fn write_file(path: &str, data: &str) -> io::Result<()> {
    fs::write(&path, &data)?;

    Ok(())
}

pub fn append_file(path: &str, data: &str) -> io::Result<()> {
    let mut append = fs::OpenOptions::new()
        .write(true)
        .append(true)
        .create(true)
        .open(&path)?;

    writeln!(append, "{}", data)?;

    Ok(())
}

pub fn write_json<T>(path: &str, data: &T) -> Result<(), Box<dyn error::Error>>
where
    T: Serialize,
{
    let data = serde_json::to_string(&data)?;

    write_file(&path, &data)?;

    Ok(())
}

pub fn read_file(path: &str) -> io::Result<String> {
    let data: String = fs::read_to_string(&path)?;

    Ok(data)
}

pub fn read_json<T>(path: &str) -> Result<T, Box<dyn error::Error>>
where
    T: for<'a> Deserialize<'a>,
{
    let data: String = read_file(&path)?;

    let data: T = serde_json::from_str(data.as_str())?;

    Ok(data)
}
