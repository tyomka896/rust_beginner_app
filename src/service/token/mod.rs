mod token_file;

use chrono::{DateTime, Duration, Utc};
use reqwest::header::{self, HeaderMap};
use serde::{Deserialize, Serialize};

use crate::utils::Log;
pub use token_file::TokenFile;

static EXPIRES_IN_MIN: i64 = 60;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Token {
    value: String,
    expires_at: Option<String>,
}

impl Token {
    pub fn from(value: &str, expires_in: Option<&str>) -> Token {
        let expires_at: Option<String> = expires_in.and_then(|value| {
            let parsed_value = if let Ok(seconds) = value.parse::<i64>() {
                Utc::now().fixed_offset() + Duration::seconds(seconds)
            } else {
                DateTime::parse_from_rfc3339(value)
                    .or_else(|error| {
                        Log::warn(&format!(
                            "Invalid parameter: expires_in - {}. Set to current time.",
                            error
                        ));

                        Err(error)
                    })
                    .unwrap_or(Utc::now().fixed_offset() + Duration::seconds(EXPIRES_IN_MIN))
            };

            Some(parsed_value.to_rfc3339().to_owned())
        });

        Token {
            value: value.to_owned(),
            expires_at,
        }
    }

    pub fn get_value(&self) -> &str {
        self.value.as_str()
    }

    pub fn is_expired(&self) -> bool {
        match self.expires_at.to_owned() {
            Some(value) => DateTime::parse_from_rfc3339(value.as_str()).unwrap() < Utc::now(),
            None => false,
        }
    }

    pub fn headers(&self) -> HeaderMap {
        let mut headers_map = HeaderMap::new();

        let token = format!("Bearer {}", &self.value);

        headers_map.insert(
            header::AUTHORIZATION,
            header::HeaderValue::from_str(token.as_str()).unwrap(),
        );

        headers_map.insert(
            header::ACCEPT,
            header::HeaderValue::from_str("application/json").unwrap(),
        );

        headers_map
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::FixedOffset;

    #[test]
    fn token_with_expires_in_as_number() {
        let value: &str = "1234567890";
        let expires_in: &str = "60";

        let token: Token = Token::from(&value, Some(&expires_in));

        assert_eq!(token.is_expired(), false);
    }

    #[test]
    fn token_with_expires_in_as_timestamp() {
        let value: &str = "1234567890";
        let expires_in: DateTime<FixedOffset> = Utc::now().fixed_offset() + Duration::minutes(1);

        let token: Token = Token::from(&value, Some(expires_in.to_rfc3339().as_str()));

        assert_eq!(token.is_expired(), false);
    }

    #[test]
    fn token_headers() {
        let value: &str = "1234567890";
        let expires_in: DateTime<FixedOffset> = Utc::now().fixed_offset();

        let token: Token = Token::from(&value, Some(expires_in.to_rfc3339().as_str()));

        let mut headers_map: HeaderMap = HeaderMap::new();

        headers_map.insert(
            header::AUTHORIZATION,
            header::HeaderValue::from_str(format!("Bearer {}", value).as_str()).unwrap(),
        );
        headers_map.insert(
            header::ACCEPT,
            header::HeaderValue::from_str("application/json").unwrap(),
        );

        assert_eq!(token.headers(), headers_map);
    }
}
