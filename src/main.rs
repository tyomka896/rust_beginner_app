#![allow(dead_code)]
// #![allow(unused_imports)]
// #![allow(unused_variables)]

mod dialog;
mod env;
mod models;
mod service;
mod utils;

use utils::Log;

use dialog::input;
use service::RestApi;

fn main() {
    let rest_api: RestApi = match dialog::connect_to_service() {
        Some(value) => value,
        None => {
            input::press_to_continue();

            return;
        }
    };

    Log::info("Connection is successful!");

    dialog::main_user_menu(&rest_api);

    input::press_to_continue();
}
