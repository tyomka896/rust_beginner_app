use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct User {
    #[serde(rename = "id")]
    pub id: i32,
    pub login: String,
    pub first_name: String,
    pub last_name: String,
    pub patronym: String,
    password: Option<String>,
}

impl User {
    pub fn _compare_password() -> bool {
        true
    }

    pub fn full_name(&self) -> String {
        format!(
            "{} {} {}",
            &self.last_name, &self.first_name, &self.patronym
        )
    }
}
