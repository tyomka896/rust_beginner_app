use std::io::{self, Write};

use crate::utils::Log;

pub fn password_not_empty(text: &str, warn_message: Option<&str>) -> Option<String> {
    let message = warn_message.unwrap_or("Empty input not allowed");

    loop {
        let value = inquire::Password::new(text)
            .without_confirmation()
            .with_display_mode(inquire::PasswordDisplayMode::Masked)
            .prompt()
            .ok()?;

        if value.len() > 0 {
            return Some(value);
        }

        Log::warn(format!("{}", message).as_str());
    }
}

pub fn input_not_empty(text: &str, warn_message: Option<&str>) -> Option<String> {
    let message = warn_message.unwrap_or("Empty input not allowed");

    loop {
        let value = inquire::Text::new(text).prompt().ok()?;

        if value.len() > 0 {
            return Some(value);
        }

        Log::warn(format!("{}", message).as_str());
    }
}

pub fn press_to_continue() {
    print!("\nPress any key to continue. . .");

    io::stdout().flush().unwrap();

    let _ = io::stdin().read_line(&mut String::new()).unwrap();
}
